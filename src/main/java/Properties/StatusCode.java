package Properties;

import lombok.Data;

@Data
public class StatusCode {
    public static Integer OK = 200;
    public static Integer BAD_REQUEST = 400;
    public static Integer NOT_FOUND = 404;
    public static Integer SERVER_ERROR = 500;
}
