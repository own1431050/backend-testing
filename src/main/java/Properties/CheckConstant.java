package Properties;

public enum CheckConstant {
    TRUE("TRUE"),
    EQUALS("EQUALS"),
    FALSE("FALSE"),
    NOT_EQUALS("NOT EQUALS");

    private final String text;

    CheckConstant(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
