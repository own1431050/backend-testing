package Requests;

import Models.Post;
import Settings.Configuration;
import io.restassured.response.Response;

import java.util.Arrays;
import java.util.List;

import static Properties.StatusCode.OK;

public class AppRequests extends Configuration {
    public Response getBasicDomain() {
        return baseGetRequest();
    }

    public List<Post> getPosts(String path) {
        return Arrays.asList(getRequest(path)
                .then()
                .statusCode(OK)
                .extract()
                .body()
                .as(Post[].class));
    }

    public Post getPost(String path) {
        return getRequest(path)
                .then()
                .statusCode(OK)
                .extract()
                .body()
                .as(Post.class);
    }
}
