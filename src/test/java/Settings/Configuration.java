package Settings;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import static io.restassured.filter.log.LogDetail.ALL;

public class Configuration extends RestAssured {
    private final String domain = "https://jsonplaceholder.typicode.com";

    private RequestSpecification requestSpecification() {
         return new RequestSpecBuilder()
                 .log(ALL)
                 .setBaseUri(domain)
                 .setContentType(ContentType.JSON)
                 .build();
    }

    private ResponseSpecification responseSpecification() {
        return new ResponseSpecBuilder()
                .log(ALL)
                .expectContentType(ContentType.JSON)
                .build();
    }

    protected Response getRequest(String path) {
        return RestAssured.given()
                .spec(requestSpecification())
                .get(domain + "/" + path);
    }

    protected Response baseGetRequest() {
        return getRequest("");
    }

    protected Response baseGetRequest(String path) {
        return getRequest(path);
    }
}
