package ApiTests;

import Models.Post;
import Requests.AppRequests;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

import static Properties.CheckConstant.TRUE;
import static Properties.StatusCode.OK;

public class CheckBaseDomainTest extends AppRequests {
    @Test(description = "Проверяем успешный ответ сервиса")
    public void checkAccessibleDomain() {
        getBasicDomain().then().statusCode(OK);
    }

    @Test(description = "Проверяем количество постов")
    public void checkAllPosts() {
        List<Post> post = getPosts("posts");
        Assert.assertEquals(post.size(), 100, TRUE.getText());
    }
}
